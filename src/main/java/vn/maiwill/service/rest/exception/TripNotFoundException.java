package vn.maiwill.service.rest.exception;

/**
 * @author LUONG VAN KHANH
 *
 */

public class TripNotFoundException extends Exception {
	
	private long trip_id;
	
	public TripNotFoundException(long trip_id) {
		super(String.format("Trip is not found with id: '%s' ", trip_id));
	}
}
