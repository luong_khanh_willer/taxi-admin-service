package vn.maiwill.service.jwtauthentication.repository;

/**
 * @author LUONG VAN KHANH
 *
 */

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.maiwill.service.jwtauthentication.model.Role;
import vn.maiwill.service.jwtauthentication.model.RoleName;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}