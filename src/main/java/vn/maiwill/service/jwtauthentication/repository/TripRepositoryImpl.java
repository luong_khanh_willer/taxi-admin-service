package vn.maiwill.service.jwtauthentication.repository;

/**
 * @author LUONG VAN KHANH
 *
 */

import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.maiwill.service.jwtauthentication.model.Trip;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Repository
public class TripRepositoryImpl { // implements TripRepository {
	
	// TODO Auto-generated method stub
}




/*	
List<Trip> findByTripCompleted(String code, String name) {
	
}

List<Trip> findByTripCanceled(String code, String name) {
	
}

List<Trip> findByTotalTrip(String code, String name) {
	
}
*/


/*
 * private static final Logger logger =
 * LoggerFactory.getLogger(TripRepositoryImpl.class);
 * 
 * @PersistenceContext private EntityManager entityManager;
 * 
 * public List<Trip> getAllTrips() {
 * 
 * 
 * }
 */

//String getDataChecking;

		/*
		Session session = this.sessionFactory.getCurrentSession();
		
		List<Trip> tripList = session.createQuery("from Trip").list();
		
		for(Trip trip : tripList){
			logger.info("Trip of List::"+ trip);
		}
		return tripList;
		*/
		
/*		
		String getDataChecking = "SELECT"
				+ 		" trip.id, "								
				+ 		" trip.pickup_latitude, "
				+ 		" trip.pickup_longitude, "				
				+ 		" trip.start_address, "
				+ 		" trip.canceled_by, "
				+ 		" trip.actual_price, "				
				+ 		" trip.estimated_distance, "				
				+ 		" trip.estimated_time, "				
				+ 		" trip.estimated_price, "
				+ 		" trip.actual_time, "				
				+ 		" trip.fee "				
				+ 	"FROM "
				+ 		"Trip as trip";	
				/*
				+ 	"INNER JOIN "
				+ 		"trip_status" 
				+	"ON" 
				+ 	"	trip.trip_status_id = trip_status.id" 
				+	"WHERE"  
				+	"    trip_status.code = 'start' ";
				*/
					
/*
		return (List<Trip>) entityManager.createQuery(getDataChecking).getResultList();
		
	}
	
	public Trip getTripById(int id) {
		return entityManager.find(Trip.class, id);
		
	}


	public List<Trip> findByOnTrip(String code) {
		
		String getDataCheckingOntrip = "SELECT"
				+ 		" trip.id, "								
				+ 		" trip.pickup_latitude, "
				+ 		" trip.pickup_longitude, "				
				+ 		" trip.start_address, "
				+ 		" trip.canceled_by, "
				+ 		" trip.actual_price, "				
				+ 		" trip.estimated_distance, "				
				+ 		" trip.estimated_time, "				
				+ 		" trip.estimated_price, "
				+ 		" trip.actual_time, "				
				+ 		" trip.fee "				
				+ 	"FROM "
				+ 		"Trip as trip";
				
				+ 	"INNER JOIN "
				+ 		"trip_status" 
				+	"ON" 
				+ 	"	trip.trip_status_id = trip_status.id" 
				+	"WHERE"  
				+	"    trip_status.code = 'start' ";
				
			
			return (List<Trip>) entityManager.createQuery(getDataCheckingOntrip).getResultList();
			*/


/*
@Transactional
@Repository
public class TripRepositoryImpl implements TripRepository {


}
*/

/*
@PersistenceContext
private EntityManager entityManager;

public Trip getTripById(int id) {
	
	return entityManager.find(Trip.class, id);
}

public List<Trip> getAllTrips() {
	
	
	String getDataChecking = "SELECT"
			+ 		" trip.id, "								
			+ 		" trip.pickup_latitude, "
			+ 		" trip.pickup_longitude, "				
			+ 		" trip.start_address, "
			+ 		" trip.canceled_by, "
			+ 		" trip.actual_price, "				
			+ 		" trip.estimated_distance, "				
			+ 		" trip.estimated_time, "				
			+ 		" trip.estimated_price, "
			+ 		" trip.actual_time, "				
			+ 		" trip.fee "				
			+ 	"FROM "
			+ 		"Trip as trip"	
			+ 	"INNER JOIN "
			+ 		"trip_status" 
			+	"ON" 
			+ 	"	trip.trip_status_id = trip_status.id" 
			+	"WHERE"  
			+	"    trip_status.code = 'start' ";

				
	

	return (List<Trip>) entityManager.createQuery(getDataChecking).getResultList();
}
*/







/*
String getDataChecking = "SELECT" + 
		"	trip.id," + 
		"	trip.start_latitude," + 
		"	trip.start_longitude," + 
		"	trip.start_address," + 
		"	trip.end_address," + 
		"	trip.actual_distance," + 
		"	trip.actual_price," + 
		"	trip.pickup_latitude," + 
		"	trip.pickup_longitude, " + 
		"	trip.fee," + 
		"	trip.canceled_by," + 
		"	trip.canceled_at," + 
		"	trip.passenger_id," + 
		"	trip.trip_type_id," + 
		"	trip.vehicle_id," + 
		"	trip.driver_id " + 
		"FROM" + 
		"	trip " + 
		"INNER JOIN" + 
		"	trip_status" + 
		"ON" + 
		"	trip.trip_status_id = trip_status.id" + 
		"WHERE" + 
		"    trip_status.code = 'start'";		
*/