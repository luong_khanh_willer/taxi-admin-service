package vn.maiwill.service.jwtauthentication.repository;

/**
 * @author LUONG VAN KHANH
 *
 */

import java.util.List;
import java.util.ArrayList;

import vn.maiwill.service.jwtauthentication.model.Trip;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Repository
public interface TripRepository extends CrudRepository<Trip, Integer> {
	
	// TODO Auto-generated method stub
	
	/*
	@Query("SELECT trip FROM Trip trip INNER JOIN trip.tripStatus ts WHERE ts.id =: id")	
	public List<Trip> findById(@Param("id") int id);
	*/
	
	
	/*
	@Query("SELECT trip FROM TripStatus ts INNER JOIN ts.trips trip WHERE ts.code = :code")
	public List<Trip> findByCode(@Param("code") String code);
	*/	
	
	
    List<Trip> findByCanceledBy(String canceledBy);
	
    // @Query("SELECT * from trip_status n join trip a on n.id = a.trip_status_id where n.code = 'p_cancel'")    
    // @Query("SELECT trip FROM Trip trip INNER JOIN trip.tripStatus ts WHERE ts.code =: code")
        
    /*
    @Query("select p from Trip p join fetch p.tripStatus o where o.code = :code")
    public List<Trip> findByCode(String code);
	*/
    
    
    
    // @Query("SELECT trip FROM Trip trip INNER JOIN trip.tripStatus ts WHERE ts.code = :code")    
    // @Query("from trip_status n join trip a on n.id = a.trip_status_id where n.code = :code")
    
    @Query("SELECT trip FROM Trip trip INNER JOIN trip.tripStatus ts WHERE ts.code = :code")
    public List<Trip> findByCode(@Param("code") String code);
    
    
    @Query("SELECT COUNT(*) FROM Trip trip INNER JOIN trip.tripStatus ts WHERE ts.code = :code")
    public Long aMethodNameOrSomething(@Param("code") String code); 
    
    
}
































// custom query to search to blog post by title or content
// List<Trip> findByTitleContainingOrContentContaining(String text, String textAgain);


/*
@Query("SELECT b FROM Blog b where b.title =: title ")
Optional<Blog> findByTripStatusCode(@Param("title")String title);
*/

// @Query("SELECT trip FROM Trip trip")
// List<Trip> findAllTrips();

/*
@Query("SELECT trip FROM Trip trip")
public Trip findById(int id);
*/

// @Query("SELECT trip FROM Trip trip INNER JOIN trip_status ON trip.trip_status_id = trip_status.id WHERE trip_status.id =: id")
// @Query("SELECT trip FROM Trip trip INNER JOIN trip.trip_status ts WHERE ts.id =: id")
// public Trip findById(@Param("id") int id);




































/*
public List<Trip> getAllTrips();

public Trip getTripById(int id);

public List<Trip> findByOnTrip(String code);
			
List<Trip> findByTripCompleted(String code, String name);

List<Trip> findByTripCanceled(String code, String name);

List<Trip> findByTotalTrip(String code, String name);	
*/	





























/*
@Query("SELECT "
			+ "trip.id, "
			+ "trip.pickup_latitude, "
			+ "trip.pickup_longitude, "
			+ "trip.start_address, "
			+ "trip.canceled_by, "
			+ "trip.actual_price, "
			+ "trip.estimated_distance, "
			+ "trip.estimated_time, "				
			+ "trip.estimated_price, "
			+ "trip.actual_time, "		
		 // + "trip.trip_status_id, "
			+ "trip.fee "
	+ "FROM "
	+ 		"Trip trip "
	+ "WHERE "
	+ 		"trip.start_address "
	+ "like  %?1")
*/


// @Query("SELECT trip.id, trip.pickup_latitude, trip.pickup_longitude, trip.start_address, trip.end_address  FROM Trip trip")
//@Query("SELECT " + QUERY_SQL + " FROM " + TABLE_NAME + " "+ TABLE_ALIAS)
// List<Trip> findAllTrips();	



// @Query("SELECT trip FROM Trip trip WHERE trip.code = :code AND trip.name = :name")
// List<Trip> findByOnTrip(@Param("code") String code, @Param("name") String name);

/*
List<Trip> findByOnTrip(String code);

List<Trip> findByTripCompleted(String code);

List<Trip> findByTripCanceled(String code);

List<Trip> findByTotalTrip(String code);
*/


// .findAll() - to get All datas
// .save()    - to save the got Data
// .delete()  - to delete the data

/*
String getDataChecking = "SELECT"
		+ 		" trip.id, "								
		+ 		" trip.pickup_latitude, "
		+ 		" trip.pickup_longitude, "				
		+ 		" trip.start_address, "
		+ 		" trip.canceled_by, "
		+ 		" trip.actual_price, "				
		+ 		" trip.estimated_distance, "				
		+ 		" trip.estimated_time, "				
		+ 		" trip.estimated_price, "
		+ 		" trip.actual_time, "				
		+ 		" trip.fee "				
		+ 	"FROM "
		+ 		"Trip as trip"	
		+ 	"INNER JOIN "
		+ 		"trip_status" 
		+	"ON" 
		+ 	"	trip.trip_status_id = trip_status.id" 
		+	"WHERE"  
		+	"    trip_status.code = 'start' ";
*/