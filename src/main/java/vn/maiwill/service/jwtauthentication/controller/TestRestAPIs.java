package vn.maiwill.service.jwtauthentication.controller;

/**
 * @author LUONG VAN KHANH
 *
 */

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import vn.maiwill.service.jwtauthentication.model.Trip;
import vn.maiwill.service.jwtauthentication.security.services.TripService;
import vn.maiwill.service.jwtauthentication.repository.TripRepository;
import vn.maiwill.service.rest.exception.TripNotFoundException;

@RestController
public class TestRestAPIs {
	
	// TODO Auto-generated method stub
	
	@Autowired
	private TripService tripService;
	
	/*
	 * GET ALL TRIP 
	 * */
	@RequestMapping(value = "/trip", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public List<Trip> getAllTrips() {
		
		return tripService.getAllTrips();
	}
	
	/*
	 * GET ALL TRIP BY ID 
	 * */
	@RequestMapping(value = "/trip/{id}", method = RequestMethod.GET)
	public @ResponseBody Optional<Trip> getAllUsers(@PathVariable Integer id) {
	
		return tripService.getById(id);
	}
	
	/*
	 * GET ALL TRIP BY CANCEL BY 
	 * */
	@RequestMapping(value = "/tripstatus/{canceledBy}", method = RequestMethod.GET)
	public List<Trip> getPersoneByName(@PathVariable String canceledBy) {
		
		return tripService.findByCanceledBy(canceledBy);
	}
	    
	
	@RequestMapping(value = "/willer/{code}", method = RequestMethod.GET)
	public List<Trip> findByCode(@PathVariable String code) {
		
		return tripService.findByCode(code);
	}
	
	
	
	@RequestMapping(value = "/maiwill/{code}", method = RequestMethod.GET)
	public @ResponseBody Long aMethodNameOrSomething(@PathVariable String code) {
	 
		return tripService.aMethodNameOrSomething(code); 
	}	
	
    @GetMapping(path="/api/test/user")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String userAccess1() {
    	
    	
    	return ">>> User Contents!";	
    }
    
	@GetMapping("/api/test/pm")
	@PreAuthorize("hasRole('PM') or hasRole('ADMIN')")
	public String projectManagementAccess() {
		
		return ">>> Board Management Project";
	}
		
	@GetMapping("/api/test/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		
		return ">>> Admin Contents";
	}
	

	
}









































/*
@GetMapping("/trip")
@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
public List<Trip> getAllTrips(){
    // return tripRepository.findAll();
	return tripRepository.findAllTrips();
}


@GetMapping("/trip/{id}")
@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
public Trip getAllTripById(@PathVariable String id) {
	
    int tripId = Integer.parseInt(id);
    
    // return tripRepository.findById(tripId);                
    return tripRepository.findById(tripId);
}
*/





















































/*
@GetMapping(path="/api/test/user")
@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
public List<Trip> getAllTrips() {} */

/*
@GetMapping(path="/api/test/user/{code}")
@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
public List<Trip> findByOnTrip(String code) {
	
	return tripRepository.findByOnTrip(code);
}
*/

/*
@GetMapping(path="/api/test/user")
@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
public String userAccess1() {
	
	
	return ">>> User Contents!";	
}
*/

/*	
@GetMapping("/api/test/user", produces = "application/json")
@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
public List<Trip> userAccess() {
			
	return tripService.getAllTrips();
}
*/




/*
public ResponseEntity<Trip> getTripById(@PathVariable("id") Integer id) {
	
	Trip trip = tripService.getTripById(id);
	
	return new ResponseEntity<Trip>(trip, HttpStatus.OK);			
}
*/	



/*
@GetMapping("/trip/{id}")
public Trip getTripById(@PathVariable(value="id") Integer tripID) throws TripNotFoundException {
	
	return tripRepository.findById(tripID).orElseThrow(() -> new TripNotFoundException(tripID));
	
}
*/


