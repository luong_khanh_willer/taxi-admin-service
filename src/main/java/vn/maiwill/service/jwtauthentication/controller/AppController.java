/**
 * 
 */
package vn.maiwill.service.jwtauthentication.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;

/**
 * @author LUONG VAN KHANH
 *
 */

@Controller
public class AppController {

	@RequestMapping("/test")
	public String test() {
		
		System.out.println("AppController -> test()");
		
		return "test.jsp";
	}
}
