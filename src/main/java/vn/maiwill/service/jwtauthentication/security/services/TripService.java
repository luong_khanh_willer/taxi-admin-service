package vn.maiwill.service.jwtauthentication.security.services;

/**
 * @author LUONG VAN KHANH
 *
 */

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import vn.maiwill.service.jwtauthentication.model.Trip;
import vn.maiwill.service.jwtauthentication.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TripService {
	
	// TODO Auto-generated method stub
	@Autowired	
	public TripRepository tripRepository;
	
	@Transactional
	public List<Trip> getAllTrips() {
		
		return (List<Trip>) tripRepository.findAll();
	}	
	
	@Transactional
	public Optional<Trip> getById(Integer id) {
		return tripRepository.findById(id);
	}
	
	@Transactional
	public List<Trip> findByCanceledBy(String canceledBy) {
		return tripRepository.findByCanceledBy(canceledBy);
	}
	
	
	@Transactional
	public List<Trip> findByCode(String code) {
		return tripRepository.findByCode(code);
	}

	
	@Transactional 
	public Long aMethodNameOrSomething(String code) {
		
		return tripRepository.aMethodNameOrSomething(code); 
	}
	
	

}
































/*	
public List<Trip> getAllTrips();

public Trip getTripById(int id);

public List<Trip> findByOnTrip(String code);

List<Trip> findByTripCompleted(String code, String name);

List<Trip> findByTripCanceled(String code, String name);

List<Trip> findByTotalTrip(String code, String name);	
*/
