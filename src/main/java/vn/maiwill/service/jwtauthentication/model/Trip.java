package vn.maiwill.service.jwtauthentication.model;

/**
 * @author LUONG VAN KHANH
 *
 */

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import vn.maiwill.service.jwtauthentication.model.TripStatus;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
// @Table(name="trip")
public class Trip implements Serializable {
	
	// TODO Auto-generated method stub	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
	
	private String start_address;
	 
	private String end_address;
	
	private String canceledBy;
	
	private Instant canceled_at;
	
	private Instant status_updated_at;
	
	private Instant created_at;
	
	private Instant updated_at;	
	
	private Instant start_at;
	
	private Instant end_at;
	
	private float pickup_latitude;
	
	private float pickup_longitude;
	
	private float fee;
		
	private float actual_distance;
		
	private float actual_price;		
	
	private float estimated_distance;
	
	private float estimated_time;
	
	private float estimated_price;
	
	private float request_latitude;
	
	private float request_longitude;
	
	private float actual_time;
	
	private float canceled_comments;
	
	private String poly_line;
	
	private float commision;
		
	@ManyToOne
    @JoinColumn(name = "trip_status_id")
    private TripStatus tripStatus;
	
	public Trip() {
		
	}
	
	public Trip(String start_address, String end_address, String canceledBy, TripStatus tripStatus) {
		this.start_address = start_address;
		this.end_address = end_address;
		this.canceledBy = canceledBy;
		this.tripStatus = tripStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStart_address() {
		return start_address;
	}

	public void setStart_address(String start_address) {
		this.start_address = start_address;
	}

	public String getEnd_address() {
		return end_address;
	}

	public void setEnd_address(String end_address) {
		this.end_address = end_address;
	}
	
	public String getCanceledby() {
		return canceledBy;
	}

	public void setCanceledBy(String canceledBy) {
		this.canceledBy = canceledBy;
	}
	
	@JsonIgnore
	public TripStatus getTripStatus() {
		return tripStatus;
	}

	public void setTripStatus(TripStatus tripStatus) {
		this.tripStatus = tripStatus;
	}

	public float getPickup_latitude() {
		return pickup_latitude;
	}

	public void setPickup_latitude(float pickup_latitude) {
		this.pickup_latitude = pickup_latitude;
	}

	public float getPickup_longitude() {
		return pickup_longitude;
	}

	public void setPickup_longitude(float pickup_longitude) {
		this.pickup_longitude = pickup_longitude;
	}

	public float getFee() {
		return fee;
	}

	public void setFee(float fee) {
		this.fee = fee;
	}

	public Instant getCanceled_at() {
		return canceled_at;
	}

	public void setCanceled_at(Instant canceled_at) {
		this.canceled_at = canceled_at;
	}

	public float getActual_distance() {
		return actual_distance;
	}

	public void setActual_distance(float actual_distance) {
		this.actual_distance = actual_distance;
	}

	public float getActual_price() {
		return actual_price;
	}

	public void setActual_price(float actual_price) {
		this.actual_price = actual_price;
	}

	public String getCanceledBy() {
		return canceledBy;
	}

	public float getEstimated_distance() {
		return estimated_distance;
	}

	public void setEstimated_distance(float estimated_distance) {
		this.estimated_distance = estimated_distance;
	}

	public float getEstimated_time() {
		return estimated_time;
	}

	public void setEstimated_time(float estimated_time) {
		this.estimated_time = estimated_time;
	}

	public float getEstimated_price() {
		return estimated_price;
	}

	public void setEstimated_price(float estimated_price) {
		this.estimated_price = estimated_price;
	}

	public float getActual_time() {
		return actual_time;
	}

	public void setActual_time(float actual_time) {
		this.actual_time = actual_time;
	}

	public Instant getStatus_updated_at() {
		return status_updated_at;
	}

	public void setStatus_updated_at(Instant status_updated_at) {
		this.status_updated_at = status_updated_at;
	}

	public Instant getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Instant created_at) {
		this.created_at = created_at;
	}

	public Instant getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Instant updated_at) {
		this.updated_at = updated_at;
	}

	public float getRequest_latitude() {
		return request_latitude;
	}

	public void setRequest_latitude(float request_latitude) {
		this.request_latitude = request_latitude;
	}

	public float getRequest_longitude() {
		return request_longitude;
	}

	public void setRequest_longitude(float request_longitude) {
		this.request_longitude = request_longitude;
	}

	public float getCanceled_comments() {
		return canceled_comments;
	}

	public void setCanceled_comments(float canceled_comments) {
		this.canceled_comments = canceled_comments;
	}

	public String getPoly_line() {
		return poly_line;
	}

	public void setPoly_line(String poly_line) {
		this.poly_line = poly_line;
	}

	public float getCommision() {
		return commision;
	}

	public void setCommision(float commision) {
		this.commision = commision;
	}

	public Instant getStart_at() {
		return start_at;
	}

	public void setStart_at(Instant start_at) {
		this.start_at = start_at;
	}

	public Instant getEnd_at() {
		return end_at;
	}

	public void setEnd_at(Instant end_at) {
		this.end_at = end_at;
	}
	
	

}



































/*
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private long id;
   
private String start_address;
 
private String end_address;
  
@ManyToOne (cascade=CascadeType.ALL)
private Department department;


Trip() {
	
}

Trip(String start_address, String end_address) {
	this.start_address = start_address;
	this.end_address = end_address;
}
 

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public String getStart_address() {
	return start_address;
}

public void setStart_address(String start_address) {
	this.start_address = start_address;
}

public String getEnd_address() {
	return end_address;
}

public void setEnd_address(String end_address) {
	this.end_address = end_address;
}

public Department getDepartment() {
	return department;
}

public void setDepartment(Department department) {
	this.department = department;
}



 @Override
 public String toString() {
    return "Trip [id=" + id + ", start_address=" + start_address + ",  end_address= " + end_address + "]";
 }
 */













































/*
 	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
    private int id;
	
	@Column(name="start_address")
    private String start_address;
	
	@Column(name="end_address")	
	private String end_address;
	
	@Column(name="pickup_latitude")
	private float pickup_latitude;
	
	@Column(name="pickup_longitude")
	private float pickup_longitude;
	
	@Column(name="fee")
	private float fee;
	
	@Column(name="canceled_by")
	private String canceledBy;	

	@Column(name="canceled_at")
	private Instant canceled_at;		
	
	@Column(name="actual_distance")
	private float actual_distance;
	
	@Column(name="actual_price")
	private float actual_price;		
	
	/////////////////////////////////////////////
	@Column(name="estimated_distance")
	private float estimated_distance;

	@Column(name="estimated_time")
	private float estimated_time;

	@Column(name="estimated_price")
	private float estimated_price;

	@Column(name="actual_time")
	private float actual_time;
	
	
	
	 @JoinColumn(name="trip_status_id")	 
	 @ManyToOne(optional = false)	 
	 @NotNull private TripStatus tripStatus;
	
		
	
	public Trip() {
		
	}
	
	public Trip(String start_address, String end_address, 
				float pickup_latitude, float pickup_longitude, 
				float fee, String canceledBy, Instant canceled_at, 
			float actual_distance, float actual_price ) {
		
		this.start_address = start_address;
		this.end_address = end_address;
		this.pickup_latitude = pickup_latitude;
		this.pickup_longitude = pickup_longitude;
		this.fee = fee;
		this.canceledBy = canceledBy;
		this.canceled_at = canceled_at;
		this.actual_distance = actual_distance;
		this.actual_price = actual_price;
		// this.tripStatus = tripStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStart_address() {
		return start_address;
	}

	public void setStart_address(String start_address) {
		this.start_address = start_address;
	}

	public String getEnd_address() {
		return end_address;
	}

	public void setEnd_address(String end_address) {
		this.end_address = end_address;
	}

	public float getPickup_latitude() {
		return pickup_latitude;
	}

	public void setPickup_latitude(float pickup_latitude) {
		this.pickup_latitude = pickup_latitude;
	}

	public float getPickup_longitude() {
		return pickup_longitude;
	}

	public void setPickup_longitude(float pickup_longitude) {
		this.pickup_longitude = pickup_longitude;
	}

	public float getFee() {
		return fee;
	}

	public void setFee(float fee) {
		this.fee = fee;
	}

	public String getCanceledby() {
		return canceledBy;
	}

	public void setCanceledBy(String canceledBy) {
		this.canceledBy = canceledBy;
	}

	public Instant getCanceled_at() {
		return canceled_at;
	}

	public void setCanceled_at(Instant canceled_at) {
		this.canceled_at = canceled_at;
	}

	public float getActual_distance() {
		return actual_distance;
	}

	public void setActual_distance(float actual_distance) {
		this.actual_distance = actual_distance;
	}

	public float getActual_price() {
		return actual_price;
	}

	public void setActual_price(float actual_price) {
		this.actual_price = actual_price;
	}		
	
	public float getEstimated_distance() {
		return estimated_distance;
	}

	public void setEstimated_distance(float estimated_distance) {
		this.estimated_distance = estimated_distance;
	}

	public float getEstimated_time() {
		return estimated_time;
	}

	public void setEstimated_time(float estimated_time) {
		this.estimated_time = estimated_time;
	}

	public float getEstimated_price() {
		return estimated_price;
	}

	public void setEstimated_price(float estimated_price) {
		this.estimated_price = estimated_price;
	}

	public float getActual_time() {
		return actual_time;
	}

	public void setActual_time(float actual_time) {
		this.actual_time = actual_time;
	}
	
	

	
	@Override
	public String toString() {
		return String.format(
				"Trip [id=%s, start_address=%s, end_address=%s, fee=%s]", 
				id,start_address,end_address, fee);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Trip other = (Trip) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}


*/



