package vn.maiwill.service.jwtauthentication.model;

/**
 * @author LUONG VAN KHANH
 *
 */

public enum  RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}