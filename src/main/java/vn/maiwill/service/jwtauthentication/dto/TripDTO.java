/**
 * 
 */
package vn.maiwill.service.jwtauthentication.dto;

/**
 * @author LUONG VAN KHANH
 *
 */
public class TripDTO {
	
	private Integer id;
	private String start_address;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getStart_address() {
		return start_address;
	}
	
	public void setStart_address(String start_address) {
		this.start_address = start_address;
	}

}
